package org.minbox.chapter;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 恒宇少年
 */
@Configuration
@ConfigurationProperties(prefix = "config")
@Data
public class RandomValuesConfig {
    /**
     *
     */
    private int number;
    private int maxNumber;
    private Long longValue;
    private Long maxLongValue;
    private String uuid;
}
