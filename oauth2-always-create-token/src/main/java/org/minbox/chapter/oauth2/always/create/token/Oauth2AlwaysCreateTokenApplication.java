package org.minbox.chapter.oauth2.always.create.token;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * curl -X POST -u "local:123456" http://localhost:9091/oauth/token -d "grant_type=password&username=hengboy&password=123456"
 */
@SpringBootApplication
public class Oauth2AlwaysCreateTokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2AlwaysCreateTokenApplication.class, args);
    }

}
